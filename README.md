# Browser Extensions for MathML

The [MathML Association](http://mathml-association.org) collects [projects for MathML specific browser extensions](/master/issues/) on this repository as GitLab issues.

The intent of this list is to prioritize implementation, organize funding, and connect implementors and users (see the [MathML Manifesto](http://mathml-association.org/legal-documents/manifesto.html) and the [charter](http://mathml-association.org/legal-documents/charter.html) of the MathML Association).

Feel free to
* [add your MathML brower extension project](/master/issues/new),
* to join the discussion on [any of existing projects](/master/issues/); we value your expertise
* volunteer to help implementing an extension
* make a bid for implementing a particular feature (prices and fees) 
* offer supervision, provide a plan of attack, or generate test cases for an extension
* express your interest in having this particular feature and describing use cases
